/*
module.exports = {
    url: 'mongodb://tttzone:k4rt45ur4@ds125126.mlab.com:25126/mongo-naru-app'
}
Mlab => 'mongodb://tttzone:k4rt45ur4@ds125126.mlab.com:25126/mongo-naru-app'
Playcourt => 'mongodb://tttzone:k4rt45ur4@naru-mongo.apps.playcourt.id:30655/mongo-naru-app'
*/

'use strict';
var Promise = require('bluebird');

module.exports = {
  // MongoDB connection options
    url: 'mongodb://tttzone:k4rt45ur4@naru-mongo.apps.playcourt.id:30655/mongo-naru-app',
    options: {
      promiseLibrary: Promise
    }
};
