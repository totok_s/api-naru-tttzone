var express = require('express');
var bodyParser = require('body-parser');

// Configuring the database
var dbConfig = require('./config/database.config.js');
var mongoose = require('mongoose');

var http= require ('http');
var path = require ('path');


mongoose.connect(dbConfig.url);

mongoose.connection.on('error', function() {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});
mongoose.connection.once('open', function() {
    console.log("Successfully connected to the database");
})


// create express app
var app = express();

app.set('port', (process.env.PORT || 5000));

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

// Require Notes routes
require('./app/routes/note.routes.js')(app);

// Require Charity routes - NARU-47
require('./app/routes/charity.routes.js')(app);

// Require News-Event routes - NARU-40
require('./app/routes/news-event.routes.js')(app);

// Require News-Event routes - NARU-40
//require('./app/routes/news-event-promo.routes.js')(app);

// Require API for Push List Lokasi routes - NARU-52
require('./app/routes/push-location.routes.js')(app);

// Require API view volunteer routes - NARU-38
require('./app/routes/volunteer.routes.js')(app);

// Require API view volunteer routes - NARU-2
require('./app/routes/config-news.routes.js')(app);

// define a simple route
app.get('/', function(req, res){
    res.json({"message": "Welcome to EasyNotes application. Take notes quickly. Organize and keep track of all your notes."});
});

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
