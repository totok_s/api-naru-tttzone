var mongoose = require('mongoose');

var CharitySchema = mongoose.Schema({
    charityTitle: {
        type: String,
        required: true,
        validate: {
            validator: function(v) {
                return /^[ \w]{3,}([A-Za-z]\.)?([ \w]*\#\d+)?(\r\n| )[ \w]{3,}/.test(v);
            },
            message: 'Please use only letters (a-z or A-Z) or numbers (0-9) or spaces and # only in this field.'
        }
    },
    charityDescription: {
        type: String,
        required: true
    },
    charityImage: {
        type: String,
        required: true
    },
    charityAuthor: {
        type: String,
        required: true 
    },
    charityLocation: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('Charity', CharitySchema);
