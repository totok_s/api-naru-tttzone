var mongoose = require('mongoose');

var ConfigNewsSchema = mongoose.Schema({
    limitpromo: {
        type: String,
        required: true
    },
    limitnews: {
        type: String,
        required: true
    },
    limitvolunteer: {
        type: String,
        required: true
    },
    limittelkomvolunteer: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('ConfigNews', ConfigNewsSchema);
