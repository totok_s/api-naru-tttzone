var mongoose = require('mongoose');

var News_eventSchema = mongoose.Schema({
    newsTitle: {
        type: String,
        required: true 
    },
    newsDescription: {
        type: String,
        required: true
    },
    newsImage: {
        type: String,
        required: true
    },
    newsAuthor: {
        type: String,
        required: true
    },
    newsLocation: {
        type: String
    },
    newsCategory: {
        type: String,
        required: true
    },
    newsStartDate: {
        type: Date
    },
    newsEndDate: {
        type: Date
    },
    newsDisasterImpact: {
        type: String
    },
    phoneNumber: {
        type: String
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('News_event', News_eventSchema);
