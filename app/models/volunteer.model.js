var mongoose = require('mongoose');

var VolunteerSchema = mongoose.Schema({
    token: {
        type: String 
    },
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        validate: {
            validator: function(v) {
                return /^([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*@([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*\.(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]){2,})$/i.test(v);
            },
            message: 'Please enter a valid email address. For example johndoe@domain.com.'
        }
    },
    phoneNumber: {
        type: String
    },
    instansi:{
        type: String
    },
    event_id:{
        type: String
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('Volunteer', VolunteerSchema);
