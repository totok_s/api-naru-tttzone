module.exports = function(app) {

    var volunteer = require('../controllers/volunteer.controller.js');

    // Create a new volunteer
    app.post('/volunteer', volunteer.create);

    // Retrieve all volunteer
    app.get('/volunteer', volunteer.findAll);

    // Retrieve a single volunteer with volunteerId
    app.get('/volunteer/:volunteerId', volunteer.findOne);
 
}
