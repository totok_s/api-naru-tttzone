module.exports = function(app) {

    var news_event = require('../controllers/news-event.controller.js');

    // Create a new Charity
    app.post('/news_event', news_event.create);

    // Retrieve all Charity
    app.get('/news_event', news_event.findAll);

    // Retrieve all Charity
    app.get('/news_event_top', news_event.findTop);

    // Retrieve all Charity Location
    app.get('/news_event_location', news_event.findLocation);

    // Retrieve a single Charity with charityId
    app.get('/news_event/:newsId', news_event.findOne);

    // Retrieve a single Charity with charityId
    app.get('/news_event_share/:newsId', news_event.findShare);

    // Update a Charity with charityId
    app.put('/news_event/:newsId', news_event.update);

    // Delete a Charity with charityId
    app.delete('/news_event/:newsId', news_event.delete);
}
