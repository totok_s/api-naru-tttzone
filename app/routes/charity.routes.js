module.exports = function(app) {

    var charities = require('../controllers/charity.controller.js');

    // Create a new Charity
    app.post('/charities', charities.create);

    // Retrieve all Charity
    app.get('/charities', charities.findAll);

    // Retrieve all Charity Telkom
    app.get('/charities_telkom', charities.findCharTelkom);

    // Retrieve a single Charity with charityId
    app.get('/charities/:charityId', charities.findOne);

    // Update a Charity with charityId
    app.put('/charities/:charityId', charities.update);

    // Delete a Charity with charityId
    app.delete('/charities/:charityId', charities.delete);
}
