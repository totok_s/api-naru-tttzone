module.exports = function(app) {

    var config_news = require('../controllers/config-news.controller.js');

    // Create a new Charity
    app.post('/config_news', config_news.create);

    // Retrieve all Charity
    app.get('/config_news', config_news.findAll);

    // Update a Charity with charityId
    app.put('/config_news/:configId', config_news.update);

}
