module.exports = function(app) {

    var push_location = require('../controllers/push-location.controller.js');

    // Update a push_location with push_locationId
    app.put('/push_location/:newsEventId', push_location.update);

}
