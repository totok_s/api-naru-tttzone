var Volunteer = require('../models/volunteer.model.js');

exports.create = function(req, res) {
    // Create and Save a new Note
    /*if(!req.body.content) {
        res.status(400).send({message: "Note can not be empty"});
    }*/

    var volunteer = new Volunteer({token: req.body.token, username: req.body.username, email: req.body.email, phoneNumber: req.body.phoneNumber, instansi: req.body.instansi, event_id: req.body.event_id});

    volunteer.save(function(err, data) {
        //console.log(data);
        if(err) {
            //console.log(err);
            res.status(500).send({message: err});
        } else {
            res.status(200).send({message: data});
        }
    });
};


exports.findAll = function(req, res) {
    // Retrieve and return all notes from the database.
    Volunteer.find(function(err, volunteer){
        if(err) {
            res.status(500).send({message: err});
        } else {
            res.status(200).send(volunteer);
        }
    });
};

exports.findOne = function(req, res) {
    // Find a single note with a noteId
    Volunteer.findById(req.params.volunteerId, function(err, data) {
        if(err) {
            res.status(500).send({message: "Could not retrieve Volunteer with id " + req.params.volunteerId});
        } else {
            res.status(200).send(data);
        }
    });
};
