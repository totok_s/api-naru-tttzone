var Config_news = require('../models/config-news.model.js');

exports.create = function(req, res) {

    var config_news = new Config_news({
                            limitpromo: req.body.limitpromo,
                            limitnews: req.body.limitnews,
                            limitvolunteer: req.body.limitvolunteer,
                            limittelkomvolunteer: req.body.limittelkomvolunteer
                          });

    config_news.save(function(err, data) {
        if(err) { 
            res.status(500).send({message: err});
        } else {
            res.status(200).send(data);
        }
    });
};

exports.findAll = function(req, res) {
    Config_news.find(function(err, config_news){
        if(err) {
            res.status(500).send({message: "Some error occurred while retrieving config_news."});
        } else {
            res.status(200).send(config_news);
        }
    });
};

exports.update = function(req, res) {
    Config_news.findById(req.params.configId, function(err, config_news) {
        if(err) {
            res.status(400).send({message: "Could not find a News_event with id " + req.params.configId});
        }

        config_news.limitpromo = req.body.limitpromo;
        config_news.limitnews=req.body.limitnews;
        config_news.limitvolunteer = req.body.limitvolunteer;
        config_news.limittelkomvolunteer = req.body.limittelkomvolunteer;

        config_news.save(function(err, data){
            if(err) {
                res.status(400).send({message: "Could not update Config_news with id " + req.params.configId});
            } else {
                res.status(200).send(data);
            }
        });
    });
};
