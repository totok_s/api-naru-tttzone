var Charity = require('../models/charity.model.js');
var News_event = require('../models/news-event.model.js');
var Config_news = require('../models/config-news.model.js');

exports.create = function(req, res) {
    // Create and Save a new Note
    /*if(!req.body.content) {
        res.status(400).send({message: "charity can not be empty"});
    }*/

    var charity = new Charity({charityTitle: req.body.charityTitle,
                            charityDescription: req.body.charityDescription,
                            charityImage: req.body.charityImage,
                            charityAuthor: req.body.charityAuthor,
                            charityLocation: req.body.charityLocation
                          });

    charity.save(function(err, data) {
        //console.log(data);
        if(err) {
            //console.log(err);
            res.status(500).send({message: err});
        } else {
            res.status(200).send(data);
        }
    });
};


exports.findAll = function(req, res) {

  Config_news.find(function(err, config_news){
      if(err) {
          res.status(500).send({message: "Some error occurred while retrieving config_news."});
      } else {
        var dinamis_limit = parseInt(config_news[0].limitvolunteer);
        // Retrieve and return all notes from the database.
        News_event.find({"newsCategory":"bencana"}).sort({newsStartDate: -1}).populate({path : 'charity'}).limit(dinamis_limit).exec(function(err, charity){
            if(err) {
                res.status(500).send({message: "Some error occurred while retrieving charity."});
            } else {
                res.status(200).send(charity);
            }
        });
      }
  });

};

exports.findCharTelkom = function(req, res) {
    // Retrieve and return all notes from the database.
    Config_news.find(function(err, config_news){
        if(err) {
            res.status(500).send({message: "Some error occurred while retrieving config_news."});
        } else {
          var dinamis_limit = parseInt(config_news[0].limitvolunteer);
          // Retrieve and return all notes from the database.
          News_event.find({ "newsCategory" : "charity-telkom"}).sort({newsStartDate: -1}).populate({path : 'charity'}).limit(dinamis_limit).exec(function(err, charity){
              if(err) {
                  res.status(500).send({message: "Some error occurred while retrieving charity."});
              } else {
                  res.status(200).send(charity);
              }
          });
        }
    });

};

exports.findOne = function(req, res) {
    // Find a single note with a noteId
    News_event.findById(req.params.newsId, function(err, data) {
        if(err) {
            res.status(500).send({message: "Could not retrieve charity with id " + req.params.newsId});
        } else {
            res.status(200).send(data);
        }
    });
};

exports.update = function(req, res) {
    // Update a note identified by the noteId in the request
    Charity.findById(req.params.charityId, function(err, charity) {
        if(err) {
            res.status(500).send({message: "Could not find a note with id " + req.params.charityId});
        }

        charity.charityTitle = req.body.charityTitle;
        charity.charityDescription=req.body.charityDescription;
        charity.charityImage = req.body.charityImage;
        charity.charityAuthor = req.body.charityAuthor;
        charity.charityLocation = req.body.charityLocation;

        charity.save(function(err, data){
            if(err) {
                res.status(500).send({message: "Could not update charity with id " + req.params.charityId});
            } else {
                res.status(200).send(data);
            }
        });
    });
};

exports.delete = function(req, res) {
    // Delete a note with the specified noteId in the request
    Charity.remove({_id: req.params.charityId}, function(err, data) {
        if(err) {
            res.status(500).send({message: "Could not delete charity with id " + req.params.id});
        } else {
            res.send({message: "charity deleted successfully!"})
        }
    });
};
