var News_event = require('../models/news-event.model.js');
var Config_news = require('../models/config-news.model.js');

exports.create = function(req, res) {
    // Create and Save a new Note
    /*if(!req.body.content) {
        res.status(400).send({message: "charity can not be empty"});
    }*/

    var news_event = new News_event({newsTitle: req.body.newsTitle,
                                      newsDescription: req.body.newsDescription,
                                      newsImage: req.body.newsImage,
                                      newsAuthor: req.body.newsAuthor,
                                      newsLocation: req.body.newsLocation,
                                      newsCategory: req.body.newsCategory,
                                      newsStartDate: req.body.newsStartDate,
                                      newsEndDate: req.body.newsEndDate,
                                      newsDisasterImpact: req.body.newsDisasterImpact,
                                      phoneNumber: req.body.phoneNumber
                                    });

    news_event.save(function(err, data) {
        //console.log(data);
        if(err) {
            //console.log(err);
            res.status(400).send({message: err});
        } else {
            res.status(200).send(data);
        }
    });
};


exports.findAll = function(req, res) {
  Config_news.find(function(err, config_news){
      if(err) {
          res.status(500).send({message: "Some error occurred while retrieving config_news."});
      } else {
        var dinamis_limit = parseInt(config_news[0].limitnews);

          News_event.find({"newsCategory":{$ne: "charity-telkom"}}).sort({newsStartDate: -1}).populate({path : 'news_event'}).limit(dinamis_limit).exec(function(err, news_event){
              if(err) {
                  res.status(400).send({message: "Some error occurred while retrieving News_event."});
              } else {
                  res.status(200).send(news_event);
              }
          });
      }
  });
 
};

exports.findLocation = function(req, res) {

    News_event.find({newsEndDate:{$gte:new Date()}}).populate({path : 'news_event'}).exec(function(err, news_event){
        if(err) {
            res.status(400).send({message: "Some error occurred while retrieving News_event."});
        } else {
            res.status(200).send(news_event);
        }
    });
};

exports.findTop = function(req, res) {
    // Retrieve and return Top News from the database.
    var curtime = new Date();
    //console.log(curtime);
    News_event.find({limit : 3},function(err, news_event){
        if(err) {
            res.status(400).send({message: "Some error occurred while retrieving News_event."});
        } else {
            res.status(200).send(news_event);
        }
    });
};

exports.findOne = function(req, res) {
    // Find a single note with a noteId
    News_event.findById(req.params.newsId, function(err, data) {
        if(err) {
            res.status(400).send({message: "Could not retrieve News_event with id " + req.params.newsId});
        } else {
            res.status(200).send(data);
        }
    });
};

exports.findShare = function(req, res) {
    // Find a single note with a noteId
    News_event.findById(req.params.newsId, function(err, data) {
        var title = data.newsTitle;
        var desc = data.newsDescription;
        var img = data.newsImage;
        var html = "<html><title>"+title+"</title><body><img src="+img+"> </br>"+desc+"</body></html>";
        if(err) {
            res.status(400).send({message: "Could not retrieve News_event with id " + req.params.newsId});
        } else {
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.write(html);
            res.end();
        }
    });
};

exports.update = function(req, res) {
    // Update a note identified by the noteId in the request
    News_event.findById(req.params.newsId, function(err, news_event) {
        if(err) {
            res.status(400).send({message: "Could not find a News_event with id " + req.params.newsId});
        }

        news_event.newsTitle = req.body.newsTitle;
        news_event.newsDescription=req.body.newsDescription;
        news_event.newsImage = req.body.newsImage;
        news_event.newsAuthor = req.body.newsAuthor;
        news_event.newsLocation = req.body.newsLocation;
        news_event.newsCategory = req.body.newsCategory;
        news_event.newsEndDate = req.body.newsEndDate;
        news_event.newsDisasterImpact = req.body.newsDisasterImpact;
        news_event.phoneNumber = req.body.phoneNumber;

        news_event.save(function(err, data){
            if(err) {
                res.status(400).send({message: "Could not update News_event with id " + req.params.newsId});
            } else {
                res.status(200).send(data);
            }
        });
    });
};

exports.delete = function(req, res) {
    // Delete a note with the specified noteId in the request
    News_event.remove({_id: req.params.newsEventId}, function(err, data) {
        if(err) {
            res.status(500).send({message: "Could not delete News_event with id " + req.params.id});
        } else {
            res.send({message: "News_event deleted successfully!"})
        }
    });
};
