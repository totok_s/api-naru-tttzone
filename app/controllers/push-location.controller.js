var News_event = require('../models/news-event.model.js');

exports.update = function(req, res) {
    // Update a note identified by the noteId in the request
    News_event.findById(req.params.newsEventId, function(err, news_event) {
        if(err) {
            res.status(500).send({message: "Could not find a News_event with id " + req.params.newsEventId});
        }

        news_event.newsEventLocation = req.body.newsEventLocation;

        news_event.save(function(err, data){
            if(err) {
                res.status(500).send({message: "Could not update newsEventLocation with push_locationId with id " + req.params.newsEventId});
            } else {
                res.send(data);
            }
        });
    });
};
